import { MigrationInterface, QueryRunner } from 'typeorm';

export class Password1650223027706 implements MigrationInterface {
  name = 'Password1650223027706';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "lobbies" ADD "password" character varying`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "lobbies" DROP COLUMN "password"`);
  }
}
