import { MigrationInterface, QueryRunner } from 'typeorm';

export class LobbyCreatedDate1649113644133 implements MigrationInterface {
  name = 'LobbyCreatedDate1649113644133';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "lobbies" ADD "created_at" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "lobbies" DROP COLUMN "created_at"`);
  }
}
