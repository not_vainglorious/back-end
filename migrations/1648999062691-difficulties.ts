import { MigrationInterface, QueryRunner } from 'typeorm';

export class difficulties1648999062691 implements MigrationInterface {
  name = 'difficulties1648999062691';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "difficulties" ("level" integer NOT NULL, CONSTRAINT "PK_5bc20952990cd217e6a894702ff" PRIMARY KEY ("level"))`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "difficulties"`);
  }
}
