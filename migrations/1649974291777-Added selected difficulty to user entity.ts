import {MigrationInterface, QueryRunner} from "typeorm";

export class AddedSelectedDifficultyToUserEntity1649974291777 implements MigrationInterface {
    name = 'AddedSelectedDifficultyToUserEntity1649974291777'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users" ADD "selected_difficulty_level" integer`);
        await queryRunner.query(`ALTER TABLE "users" ADD CONSTRAINT "FK_a26397a106cad749d2ed2f7362c" FOREIGN KEY ("selected_difficulty_level") REFERENCES "difficulties"("level") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users" DROP CONSTRAINT "FK_a26397a106cad749d2ed2f7362c"`);
        await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "selected_difficulty_level"`);
    }

}
