import { MigrationInterface, QueryRunner } from 'typeorm';

export class isStartedLobbyMark1650241988867 implements MigrationInterface {
  name = 'isStartedLobbyMark1650241988867';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "lobbies" ADD "is_started" boolean NOT NULL DEFAULT false`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "lobbies" DROP COLUMN "is_started"`);
  }
}
