import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddedLobbyRegion1650222536052 implements MigrationInterface {
  name = 'AddedLobbyRegion1650222536052';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TYPE "public"."lobbies_region_enum" AS ENUM('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20')`,
    );
    await queryRunner.query(
      `ALTER TABLE "lobbies" ADD "region" "public"."lobbies_region_enum" NOT NULL DEFAULT '0'`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "lobbies" DROP COLUMN "region"`);
    await queryRunner.query(`DROP TYPE "public"."lobbies_region_enum"`);
  }
}
