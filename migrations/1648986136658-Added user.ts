import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddedUser1648986136658 implements MigrationInterface {
  name = 'AddedUser1648986136658';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "users" ("steam_id" bigint NOT NULL, "name" character varying NOT NULL, "image_url" character varying NOT NULL, "image_url_medium" character varying NOT NULL, "image_url_full" character varying NOT NULL, CONSTRAINT "PK_54fe0ca3c43bc032e3333d12a90" PRIMARY KEY ("steam_id"))`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "users"`);
  }
}
