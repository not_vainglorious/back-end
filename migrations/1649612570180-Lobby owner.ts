import { MigrationInterface, QueryRunner } from 'typeorm';

export class LobbyOwner1649612570180 implements MigrationInterface {
  name = 'LobbyOwner1649612570180';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "lobby_users_link" ADD "is_owner" boolean NOT NULL DEFAULT false`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "lobby_users_link" DROP COLUMN "is_owner"`,
    );
  }
}
