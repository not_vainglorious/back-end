import { MigrationInterface, QueryRunner } from 'typeorm';

export class CascadeDelete1649613541885 implements MigrationInterface {
  name = 'CascadeDelete1649613541885';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "lobby_users_link" DROP CONSTRAINT "FK_8f7604083214390eb9e45b72dd9"`,
    );
    await queryRunner.query(
      `ALTER TABLE "lobby_users_link" ADD CONSTRAINT "FK_8f7604083214390eb9e45b72dd9" FOREIGN KEY ("lobby_id") REFERENCES "lobbies"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "lobby_users_link" DROP CONSTRAINT "FK_8f7604083214390eb9e45b72dd9"`,
    );
    await queryRunner.query(
      `ALTER TABLE "lobby_users_link" ADD CONSTRAINT "FK_8f7604083214390eb9e45b72dd9" FOREIGN KEY ("lobby_id") REFERENCES "lobbies"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
