import { MigrationInterface, QueryRunner } from 'typeorm';

export class newDifficulties1650294948480 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `INSERT INTO "difficulties" (level) VALUES (15),(16)`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `DELETE FROM "difficulties" WHERE level IN [15, 16]`,
    );
  }
}
