import { MigrationInterface, QueryRunner } from 'typeorm';

export class lobby1649026594824 implements MigrationInterface {
  name = 'lobby1649026594824';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "lobbies" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "max_players" integer NOT NULL, "difficulty_level" integer NOT NULL, CONSTRAINT "PK_93d4e0c6f527286e712d53a8a57" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TYPE "public"."lobby_users_link_role_enum" AS ENUM('CARRY', 'MAP', 'ARC', 'PUGNA', 'UNKNOWN')`,
    );
    await queryRunner.query(
      `CREATE TABLE "lobby_users_link" ("lobby_id" uuid NOT NULL, "user_steam_id" bigint NOT NULL, "role" "public"."lobby_users_link_role_enum" NOT NULL DEFAULT 'UNKNOWN', CONSTRAINT "REL_9f5dc19ba09a8db7abb6f17d2b" UNIQUE ("user_steam_id"), CONSTRAINT "PK_a94588eb83c8b89bf285087c270" PRIMARY KEY ("lobby_id", "user_steam_id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "lobbies" ADD CONSTRAINT "FK_8d037d3833c5ede541369904aa1" FOREIGN KEY ("difficulty_level") REFERENCES "difficulties"("level") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "lobby_users_link" ADD CONSTRAINT "FK_9f5dc19ba09a8db7abb6f17d2be" FOREIGN KEY ("user_steam_id") REFERENCES "users"("steam_id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "lobby_users_link" ADD CONSTRAINT "FK_8f7604083214390eb9e45b72dd9" FOREIGN KEY ("lobby_id") REFERENCES "lobbies"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "lobby_users_link" DROP CONSTRAINT "FK_8f7604083214390eb9e45b72dd9"`,
    );
    await queryRunner.query(
      `ALTER TABLE "lobby_users_link" DROP CONSTRAINT "FK_9f5dc19ba09a8db7abb6f17d2be"`,
    );
    await queryRunner.query(
      `ALTER TABLE "lobbies" DROP CONSTRAINT "FK_8d037d3833c5ede541369904aa1"`,
    );
    await queryRunner.query(`DROP TABLE "lobby_users_link"`);
    await queryRunner.query(`DROP TYPE "public"."lobby_users_link_role_enum"`);
    await queryRunner.query(`DROP TABLE "lobbies"`);
  }
}
