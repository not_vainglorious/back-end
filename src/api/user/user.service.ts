import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IsNull, Not, Repository } from 'typeorm';
import { User } from './user.type';
import { UserEntity } from './entities';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
  ) {}

  create(args: User): Promise<User> {
    return this.userRepository.save(this.userRepository.create(args));
  }

  findOne(args: Partial<User>): Promise<User | undefined> {
    return this.userRepository.findOne(args);
  }

  async get(args: Partial<User>): Promise<User> {
    const user = await this.findOne(args);

    if (!user) {
      throw new NotFoundException();
    }

    return user;
  }

  find(): Promise<Array<User>> {
    const where = {};

    return this.userRepository.find({ where });
  }

  async delete(args: Partial<User>): Promise<void> {
    await this.userRepository.delete(args);
  }

  async update(
    args: Required<Pick<User, 'steamId'>> & Partial<Omit<User, 'steamId'>>,
  ): Promise<void> {
    const { steamId, ...rest } = args;

    await this.userRepository.update({ steamId }, rest);
  }

  async updateAll(args: Partial<Omit<User, 'steamId'>>) {
    await this.userRepository.update(
      { selectedDifficultyLevel: Not(IsNull()) },
      args,
    );
  }
}
