import { ApiProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';
import { User } from './user.type';

export class UserDto implements User {
  @ApiProperty()
  public readonly steamId: string;

  @ApiProperty()
  public readonly name: string;

  @ApiProperty()
  public readonly imageUrl: string;

  @ApiProperty()
  public readonly imageUrlMedium: string;

  @ApiProperty()
  public readonly imageUrlFull: string;

  @Exclude()
  public readonly selectedDifficultyLevel?: number;

  constructor(partial: Partial<UserDto>) {
    Object.assign(this, partial);
  }
}
