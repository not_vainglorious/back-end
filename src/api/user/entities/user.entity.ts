import { DifficultyEntity } from 'src/api/difficulty/entities';
import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { User } from '../user.type';

@Entity({ name: 'users' })
export class UserEntity implements User {
  @PrimaryColumn({ name: 'steam_id', type: 'bigint' })
  public readonly steamId: string;

  @Column({ type: 'varchar', name: 'name', nullable: false })
  public readonly name: string;

  @Column({ type: 'varchar', name: 'image_url' })
  public readonly imageUrl: string;

  @Column({ type: 'varchar', name: 'image_url_medium' })
  public readonly imageUrlMedium: string;

  @Column({ type: 'varchar', name: 'image_url_full' })
  public readonly imageUrlFull: string;

  @Column({ type: 'int', name: 'selected_difficulty_level', nullable: true })
  public readonly selectedDifficultyLevel: number;

  @ManyToOne(() => DifficultyEntity)
  @JoinColumn({ name: 'selected_difficulty_level' })
  public readonly difficulty: DifficultyEntity;
}
