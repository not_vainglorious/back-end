import {
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  UseGuards,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOperation,
  ApiResponse,
  ApiBearerAuth,
} from '@nestjs/swagger';
import { AuthenticatedUser } from 'src/api/auth/decorators';
import { JwtAuthGuard } from 'src/api/auth/guards';
import { User } from './user.type';
import { UserDto } from './user.dto';
import { UserService } from './user.service';

@Controller()
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@ApiTags('User')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @ApiOperation({ summary: 'Get user' })
  @ApiResponse({ status: HttpStatus.OK, type: UserDto })
  @HttpCode(HttpStatus.OK)
  @Get('')
  getUser(@AuthenticatedUser() user: User): UserDto {
    return new UserDto(user);
  }
}
