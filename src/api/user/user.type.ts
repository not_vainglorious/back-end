import { Difficulty } from '../difficulty/difficulty.type';

export type User = {
  steamId: string;
  name: string;
  imageUrl: string;
  imageUrlMedium: string;
  imageUrlFull: string;
  selectedDifficultyLevel?: Difficulty['level'];
};
