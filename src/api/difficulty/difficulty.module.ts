import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DifficultyEntity } from './entities';
import { DifficultyController } from './difficulty.controller';
import { DifficultyService } from './difficulty.service';
import { AuthModule } from '../auth';

@Module({
  imports: [TypeOrmModule.forFeature([DifficultyEntity]), AuthModule],
  providers: [DifficultyService],
  exports: [DifficultyService],
  controllers: [DifficultyController],
})
export class DifficultyModule {}
