import { LobbyEntity } from 'src/api/lobby/entities';
import { Entity, OneToMany, PrimaryColumn } from 'typeorm';
import { Difficulty } from '../difficulty.type';

@Entity({ name: 'difficulties' })
export class DifficultyEntity implements Difficulty {
  @PrimaryColumn({ name: 'level', type: 'int' })
  public readonly level: number;

  @OneToMany(() => LobbyEntity, (lobby) => lobby.difficulty)
  public readonly lobbies: Array<LobbyEntity>;
}
