import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindConditions, Repository } from 'typeorm';
import { Difficulty } from './difficulty.type';
import { DifficultyEntity } from './entities';

@Injectable()
export class DifficultyService {
  constructor(
    @InjectRepository(DifficultyEntity)
    private readonly difficultyRepository: Repository<DifficultyEntity>,
  ) {}

  create(args: Difficulty): Promise<Difficulty> {
    return this.difficultyRepository.save(
      this.difficultyRepository.create(args),
    );
  }

  findOne(args: Partial<Difficulty>): Promise<Difficulty | undefined> {
    return this.difficultyRepository.findOne(args);
  }

  async get(args: Partial<Difficulty>): Promise<Difficulty> {
    const user = await this.findOne(args);

    if (!user) {
      throw new NotFoundException();
    }

    return user;
  }

  find(args?: FindConditions<Difficulty>): Promise<Array<Difficulty>> {
    const query = this.difficultyRepository
      .createQueryBuilder('difficulty')
      .loadRelationCountAndMap(
        'difficulty.lobbiesCount',
        'difficulty.lobbies',
        'lobby',
        (qb) =>
          qb
            .leftJoin('lobby.users', 'users')
            .where('lobby.is_started = :isStarted', { isStarted: false })
            .having('COUNT(users.user_steam_id) < lobby.max_players')
            .groupBy('lobby.max_players')
            .addGroupBy('lobby.difficulty_level'),
      );

    if (args) {
      query.where(args);
    }

    return query.getMany();
  }

  async delete(args: Partial<Difficulty>): Promise<void> {
    await this.difficultyRepository.delete(args);
  }
}
