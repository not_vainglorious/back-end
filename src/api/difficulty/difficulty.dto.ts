import { ApiProperty } from '@nestjs/swagger';
import { Difficulty } from './difficulty.type';

export class DifficultyDto implements Difficulty {
  @ApiProperty()
  public readonly level: number;

  @ApiProperty()
  public readonly lobbiesCount: number;

  constructor(partial: Partial<DifficultyDto>) {
    Object.assign(this, partial);
  }
}
