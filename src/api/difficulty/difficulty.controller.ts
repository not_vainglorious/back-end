import { Controller, Get, HttpCode, HttpStatus } from '@nestjs/common';
import {
  ApiTags,
  ApiOperation,
  ApiResponse,
  ApiBearerAuth,
} from '@nestjs/swagger';
import { DifficultyDto } from './difficulty.dto';
import { DifficultyService } from './difficulty.service';

@Controller()
@ApiBearerAuth()
@ApiTags('Difficulty')
export class DifficultyController {
  constructor(private readonly difficultyService: DifficultyService) {}

  @ApiOperation({ summary: 'Get difficulties' })
  @ApiResponse({ status: HttpStatus.OK, type: DifficultyDto, isArray: true })
  @HttpCode(HttpStatus.OK)
  @Get('')
  async getDifficulties(): Promise<Array<DifficultyDto>> {
    const difficulties = await this.difficultyService.find();

    return difficulties.map((item) => new DifficultyDto(item));
  }
}
