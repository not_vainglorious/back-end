import { Module } from '@nestjs/common';
import { RouterModule } from '@nestjs/core';
import { AuthModule } from './auth';
import { BotModule } from './bot/bot.module';
import { DifficultyModule } from './difficulty';
import { LobbyModule } from './lobby';
import { UserModule } from './user';

@Module({
  imports: [
    AuthModule,
    UserModule,
    DifficultyModule,
    LobbyModule,
    BotModule,
    RouterModule.register([
      {
        path: 'auth',
        module: AuthModule,
      },
      {
        path: 'users',
        module: UserModule,
      },
      {
        path: 'difficulties',
        module: DifficultyModule,
      },
    ]),
  ],
})
export class ApiModule {}
