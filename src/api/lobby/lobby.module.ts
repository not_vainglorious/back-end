import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from '../auth';
import { BotModule } from '../bot/bot.module';
import { DifficultyModule } from '../difficulty';
import { UserModule } from '../user';
import { LobbyUserEntity, LobbyEntity } from './entities';
import { LobbyGateway } from './lobby.gateway';
import { LobbyService } from './lobby.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([LobbyUserEntity, LobbyEntity]),
    AuthModule,
    UserModule,
    DifficultyModule,
    forwardRef(() => BotModule),
  ],
  providers: [LobbyGateway, LobbyService],
  exports: [LobbyService],
})
export class LobbyModule {}
