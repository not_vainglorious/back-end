import { Logger, UseGuards } from '@nestjs/common';
import {
  SubscribeMessage,
  WebSocketGateway,
  OnGatewayInit,
  WebSocketServer,
  OnGatewayConnection,
  OnGatewayDisconnect,
  ConnectedSocket,
  MessageBody,
  WsException,
} from '@nestjs/websockets';
import { Socket, Server } from 'socket.io';
import { LobbyService } from './lobby.service';
import { AuthService } from '../auth';
import { AuthenticatedUserWs } from '../auth/decorators';
import { JwtSocketAuthGuard } from '../auth/guards/jwt-socket.guard';
import {
  CreateLobbyPayload,
  Lobby,
  LobbyUser,
  SocketMessages,
} from '../lobby/lobby.type';
import { User } from '../user/user.type';
import { Difficulty } from '../difficulty/difficulty.type';
import { DifficultyService } from '../difficulty';
import { UserService } from '../user';
import { BotService } from '../bot/bot.service';

@WebSocketGateway({
  cors: {
    origin: '*',
  },
})
export class LobbyGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect
{
  private logger: Logger = new Logger('LobbyGateway');

  @WebSocketServer() wss: Server;

  constructor(
    private readonly lobbyService: LobbyService,
    private readonly difficultyService: DifficultyService,
    private readonly authService: AuthService,
    private readonly userService: UserService,
    private readonly botService: BotService,
  ) {}

  async afterInit() {
    await this.userService.updateAll({ selectedDifficultyLevel: null });

    this.logger.log('Initialize LobbyGateway!');
  }

  async handleConnection(@ConnectedSocket() client: Socket) {
    // Get authorization token
    const token = client.handshake.headers.authorization;

    // Disconnect user if there is no token
    if (!token) {
      client.disconnect();

      return;
    }

    // Verify token and get user steam id
    const { steamId: userSteamId, selectedDifficultyLevel } =
      await this.authService.verifyBearerToken({
        token,
      });

    // Join user personal steam id room
    client.join(getUserRoom(userSteamId));

    // If there is selected difficulty (database)
    if (selectedDifficultyLevel) {
      // Sync difficulty on new client
      client.emit(SocketMessages.DIFFICULTY_SYNC, {
        difficultyLevel: selectedDifficultyLevel,
      });
    }

    const lobbyUser = await this.lobbyService.findOneLobbyUser({ userSteamId });

    if (lobbyUser) {
      client.emit(
        SocketMessages.LOBBY_JOIN_SUCCESS,
        await this.lobbyService.getLobbyFullWithHidden({
          id: lobbyUser.lobbyId,
        }),
      );
    }
  }

  async handleDisconnect(@ConnectedSocket() client: Socket) {
    // Get authorization token
    const token = client.handshake.headers.authorization;

    if (token) {
      // Verify token and get user steam id
      const { steamId: userSteamId } = await this.authService.verifyBearerToken(
        {
          token,
        },
      );

      // Get list of connected clients to user personal room
      const room = this.wss.sockets.adapter.rooms.get(getUserRoom(userSteamId));

      // If room is emtpy (last client)
      // Disconnecting from lobby
      if (!room || !room.size) {
        await this.lobbyService.disconnectUser({ userSteamId });

        await this.userService.update({
          steamId: userSteamId,
          selectedDifficultyLevel: null,
        });
      }
    }
  }

  @UseGuards(JwtSocketAuthGuard)
  @SubscribeMessage(SocketMessages.DIFFICULTY_CHANGE)
  async handleChangeDifficulty(
    @ConnectedSocket() client: Socket,
    @MessageBody()
    difficultyLevel: number,
    @AuthenticatedUserWs() user: User,
  ) {
    const { steamId: userSteamId, selectedDifficultyLevel } = user;

    const isCurrentDifficulty = selectedDifficultyLevel === difficultyLevel;

    if (!isCurrentDifficulty) {
      // Disconnecting (database)
      await this.lobbyService.disconnectUser({
        userSteamId,
      });

      // Client leaving lobby and difficulty lobbies
      client.rooms.forEach((room) => {
        if (
          ['Difficulty', 'Lobby'].reduce(
            (acc, red) => room.startsWith(red) || acc,
            false,
          )
        ) {
          client.leave(room);
        }
      });
    }

    await this.userService.update({
      steamId: userSteamId,
      selectedDifficultyLevel: difficultyLevel,
    });

    const difficultyRoomName = getDifficultyRoom(difficultyLevel);

    // Join new difficulty room
    client.join(difficultyRoomName);

    // Get lobbies for new difficulty
    const lobbies = await this.lobbyService.findLobbies({ difficultyLevel });

    // Send to client new difficulty level and its lobbies
    client.emit(SocketMessages.DIFFICULTY_CHANGE_SUCCESS, {
      difficultyLevel,
      lobbies,
    });

    // Notify all other clients (this steam user only) without those,
    // who already still not joined new difficulty room
    this.emitUser({
      userSteamId,
      message: SocketMessages.DIFFICULTY_SYNC,
      exceptRooms: [difficultyRoomName],
      data: {
        difficultyLevel,
        lobbies,
      },
    });
  }

  @UseGuards(JwtSocketAuthGuard)
  @SubscribeMessage(SocketMessages.LOBBY_CREATE_AND_JOIN)
  async handleCreateAndJoinLobby(
    @MessageBody()
    createLobby: CreateLobbyPayload,
    @AuthenticatedUserWs() user: User,
  ) {
    const { steamId: userSteamId } = user;
    const { lobby, role } = createLobby;

    // Disconnect from current lobby
    await this.leaveLobby({ userSteamId });

    // Create lobby and join (transactional)
    const { id: lobbyId } = await this.lobbyService.createAndJoinLobby({
      lobby,
      user: { userSteamId, role },
    });

    // Join lobby room
    this.joinRoom({ userSteamId, room: getLobbyRoom(lobbyId) });

    // Send client data about created and joined lobby
    this.emitUser({
      userSteamId,
      message: SocketMessages.LOBBY_JOIN_SUCCESS,
      data: await this.lobbyService.getLobbyFullWithHidden({ id: lobbyId }),
    });

    // Trigger stats update for selected difficulty for all users
    await this.onDifficultiesStatsUpdate({
      level: lobby.difficultyLevel,
    });
  }
  @UseGuards(JwtSocketAuthGuard)
  @SubscribeMessage(SocketMessages.LOBBY_JOIN)
  async handleJoinLobby(
    @MessageBody() lobbyId: Lobby['id'],
    @AuthenticatedUserWs() user: User,
  ) {
    const { steamId: userSteamId } = user;

    // Disconnect from current lobby
    await this.leaveLobby({ userSteamId });

    // Join new lobby (database)
    await this.lobbyService.joinLobby({ userSteamId, lobbyId });

    // Join lobby room
    this.joinRoom({ userSteamId, room: getLobbyRoom(lobbyId) });

    // Send client data about created and joined lobby
    this.emitUser({
      userSteamId,
      message: SocketMessages.LOBBY_JOIN_SUCCESS,
      data: await this.lobbyService.getLobbyFullWithHidden({ id: lobbyId }),
    });
  }

  @UseGuards(JwtSocketAuthGuard)
  @SubscribeMessage(SocketMessages.LOBBY_ROLE_UPDATE)
  async handleUpdateRoleLobby(
    @MessageBody() role: LobbyUser['role'],
    @AuthenticatedUserWs() user: User,
  ) {
    const { steamId: userSteamId } = user;

    const userLobby = await this.lobbyService.findOneLobbyUser({ userSteamId });

    if (!userLobby) {
      throw new WsException('User not in lobby');
    }

    await this.lobbyService.updateLobbyRole({
      userSteamId,
      lobbyId: userLobby.lobbyId,
      role,
    });
  }

  @UseGuards(JwtSocketAuthGuard)
  @SubscribeMessage(SocketMessages.LOBBY_PLAYER_REMOVE)
  async handleRemovePlayerLobby(
    @AuthenticatedUserWs() user: User,
    @MessageBody() userId: User['steamId'],
  ) {
    const { steamId: userSteamId } = user;

    const lobbyUser = await this.lobbyService.findOneLobbyUser({
      userSteamId,
      isOwner: true,
    });

    if (!lobbyUser) {
      throw new WsException('User not found or not lobby leader');
    }

    if (
      await this.lobbyService.findOneLobbyUser({
        lobbyId: lobbyUser.lobbyId,
        userSteamId: userId,
      })
    ) {
      // Disconnect from lobby (database)
      await this.leaveLobby({ userSteamId: userId });
    }
  }

  @UseGuards(JwtSocketAuthGuard)
  @SubscribeMessage(SocketMessages.LOBBY_LEAVE)
  async handleLeaveLobby(@AuthenticatedUserWs() user: User) {
    const { steamId: userSteamId } = user;

    // Disconnect from lobby (database)
    await this.leaveLobby({ userSteamId });
  }

  @UseGuards(JwtSocketAuthGuard)
  @SubscribeMessage(SocketMessages.LOBBY_START)
  async handleStartLobby(@AuthenticatedUserWs() user: User) {
    const { steamId: userSteamId } = user;

    const lobbyUser = await this.lobbyService.findOneLobbyUser({
      userSteamId,
      isOwner: true,
    });

    const lobby = await this.lobbyService.getLobby({ id: lobbyUser.lobbyId });

    if (lobby.isStarted) {
      throw new WsException('Lobby already started');
    }

    await this.lobbyService.updateLobby({
      id: lobbyUser.lobbyId,
      isStarted: true,
    });

    await this.botService.startLobby({ id: lobby.id });

    await this.onDifficultiesStatsUpdate({ level: lobby.difficultyLevel });
  }

  async onDifficultiesStatsUpdate(args: Pick<Difficulty, 'level'>) {
    const { level } = args;
    const diffilcultyStats = await this.difficultyService.find({ level });

    this.wss.emit(SocketMessages.DIFFICULTIES_SYNC, diffilcultyStats);
  }

  async onLobbyCreate(args: Pick<Lobby, 'id'>) {
    const { id } = args;
    const lobby = await this.lobbyService.getLobbyFull({ id });

    this.emitDifficulty({
      level: lobby.difficultyLevel,
      message: SocketMessages.LOBBY_CREATE_SYNC,
      data: lobby,
    });
  }

  async onLobbyUpdate(args: Pick<Lobby, 'id'>) {
    const { id } = args;

    const lobby = await this.lobbyService.getLobbyFull({ id });
    const lobbyWithHidden = await this.lobbyService.getLobbyFullWithHidden({
      id,
    });

    this.emitDifficulty({
      level: lobby.difficultyLevel,
      message: SocketMessages.LOBBY_UPDATE_SYNC,
      exceptRooms: [getLobbyRoom(lobby.id)],
      data: lobby,
    });

    this.emitLobby({
      id: lobby.id,
      message: SocketMessages.LOBBY_UPDATE_SYNC,
      data: lobbyWithHidden,
    });
  }

  async onLobbyDelete(args: Lobby) {
    const { id, difficultyLevel: level } = args;

    this.emitDifficulty({
      level,
      message: SocketMessages.LOBBY_DELETE_SYNC,
      data: id,
    });
  }

  private emitUser<T>(args: EmitUserPayload<T>): void {
    const { userSteamId, message, exceptRooms = [], data } = args;

    this.wss.sockets
      .in(getUserRoom(userSteamId))
      .except(exceptRooms)
      .emit(message, data);
  }

  private emitDifficulty<T>(args: EmitDifficultyPayload<T>): void {
    const { level, message, exceptRooms = [], data } = args;

    this.wss.sockets
      .in(getDifficultyRoom(level))
      .except(exceptRooms)
      .emit(message, data);
  }

  private emitLobby<T>(args: EmitLobbyPayload<T>): void {
    const { id, message, exceptRooms = [], data } = args;

    this.wss.sockets
      .in(getLobbyRoom(id))
      .except(exceptRooms)
      .emit(message, data);
  }

  private leaveRooms(args: LeaveRoomsPayload): void {
    const { userSteamId, startsWith } = args;

    this.wss.sockets.adapter.rooms
      .get(getUserRoom(userSteamId))
      .forEach((client) => {
        const socket = this.wss.sockets.sockets.get(client);

        socket.rooms.forEach((room) => {
          if (
            startsWith.reduce((acc, red) => room.startsWith(red) || acc, false)
          ) {
            socket.leave(room);
          }
        });
      });
  }

  private joinRoom(args: JoinRoomPayload): void {
    const { userSteamId, room } = args;

    this.wss.sockets.adapter.rooms
      .get(getUserRoom(userSteamId))
      .forEach((userClient) => {
        const socket = this.wss.sockets.sockets.get(userClient);

        socket.join(room);
      });
  }

  private async leaveLobby(args: Pick<LobbyUser, 'userSteamId'>) {
    const { userSteamId } = args;

    const lobby = await this.lobbyService.disconnectUser({ userSteamId });

    // If user was disconnected from lobby
    // Leave all socket rooms
    if (lobby) {
      this.leaveRooms({ userSteamId, startsWith: [getLobbyRoom(lobby.id)] });

      this.emitUser({
        userSteamId,
        message: SocketMessages.LOBBY_LEAVE_SUCCESS,
      });
    }
  }
}

const getUserRoom = (userId: string) => `User:${userId}`;

const getDifficultyRoom = (difficultyLevel: number) =>
  `Difficulty:${difficultyLevel}`;

const getLobbyRoom = (lobbyId: string) => `Lobby:${lobbyId}`;

export type EmitUserPayload<T> = Record<'userSteamId', User['steamId']> &
  Record<'message', string> &
  Partial<Record<'exceptRooms', Array<string>>> &
  Partial<Record<'data', T>>;

export type EmitDifficultyPayload<T> = Pick<Difficulty, 'level'> &
  Record<'message', string> &
  Partial<Record<'exceptRooms', Array<string>>> &
  Partial<Record<'data', T>>;

export type EmitLobbyPayload<T> = Pick<Lobby, 'id'> &
  Record<'message', string> &
  Partial<Record<'exceptRooms', Array<string>>> &
  Partial<Record<'data', T>>;

export type LeaveRoomsPayload = Record<'userSteamId', User['steamId']> &
  Record<'startsWith', Array<string>>;

export type JoinRoomPayload = Record<'userSteamId', User['steamId']> &
  Record<'room', string>;
