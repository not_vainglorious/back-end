import { DifficultyEntity } from 'src/api/difficulty/entities/difficulty.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Lobby, LobbyRegion } from '../lobby.type';
import { LobbyUserEntity } from './lobby-user.entity';

@Entity({ name: 'lobbies' })
export class LobbyEntity implements Lobby {
  @PrimaryGeneratedColumn('uuid', { name: 'id' })
  public readonly id: string;

  @Column({ type: 'varchar', name: 'name', nullable: false })
  public readonly name: string;

  @Column({
    type: 'enum',
    name: 'region',
    enum: LobbyRegion,
    default: LobbyRegion.STOCKHOLM,
  })
  public readonly region: LobbyRegion;

  @Column({ type: 'varchar', name: 'password', nullable: true, select: false })
  public readonly password: string;

  @Column({ type: 'int', name: 'max_players', nullable: false })
  public readonly maxPlayers: number;

  @Column({ type: 'int', name: 'difficulty_level', nullable: false })
  public readonly difficultyLevel: number;

  @CreateDateColumn({
    type: 'timestamp',
    name: 'created_at',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  public readonly createdAt: Date;

  @Column({
    type: 'bool',
    name: 'is_started',
    default: false,
  })
  public readonly isStarted: boolean;

  @ManyToOne(() => DifficultyEntity)
  @JoinColumn({ name: 'difficulty_level' })
  public readonly difficulty: DifficultyEntity;

  @OneToMany(() => LobbyUserEntity, ({ lobby }) => lobby)
  public readonly users: LobbyUserEntity[];
}
