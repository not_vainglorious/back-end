import { UserEntity } from 'src/api/user/entities/user.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryColumn,
} from 'typeorm';
import { LobbyUser, LobbyUserRole } from '../lobby.type';
import { LobbyEntity } from './lobby.entity';

@Entity({ name: 'lobby_users_link' })
export class LobbyUserEntity implements Required<LobbyUser> {
  @PrimaryColumn({ type: 'uuid', name: 'lobby_id' })
  public readonly lobbyId: string;

  @PrimaryColumn({ type: 'bigint', name: 'user_steam_id' })
  public readonly userSteamId: string;

  @Column({
    type: 'enum',
    name: 'role',
    enum: LobbyUserRole,
    default: LobbyUserRole.UNKNOWN,
  })
  public readonly role: LobbyUserRole;

  @Column({
    type: 'bool',
    name: 'is_owner',
    default: false,
  })
  public readonly isOwner: boolean;

  @OneToOne(() => UserEntity)
  @JoinColumn({ name: 'user_steam_id' })
  public readonly user: UserEntity;

  @ManyToOne(() => LobbyEntity, {
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'lobby_id' })
  public readonly lobby: LobbyEntity;
}
