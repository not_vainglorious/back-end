import { Difficulty } from 'src/api/difficulty/difficulty.type';
import { User } from '../user/user.type';

export type Lobby = {
  id: string;
  name: string;
  region: LobbyRegion;
  isStarted: boolean;
  password?: string;
  maxPlayers: number;
  difficultyLevel: Difficulty['level'];
  createdAt: Date;
};

export enum LobbyRegion {
  'USWEST' = 1,
  'USEAST' = 2,
  'EUROPE' = 3,
  'KOREA' = 4,
  'SINGAPORE' = 5,
  'DUBAI' = 6,
  'AUSTRALIA' = 7,
  'STOCKHOLM' = 8,
  'AUSTRIA' = 9,
  'BRAZIL' = 10,
  'SOUTHAFRICA' = 11,
  'PWTELECOMSHANGHAI' = 12,
  'PWUNICOM' = 13,
  'CHILE' = 14,
  'PERU' = 15,
  'INDIA' = 16,
  'PWTELECOMGUANGZHOU' = 17,
  'PWTELECOMZHEJIANG' = 18,
  'JAPAN' = 19,
  'PWTELECOMWUHAN' = 20,
}

export enum LobbyUserRole {
  CARRY = 'CARRY',
  MAP = 'MAP',
  ARC = 'ARC',
  PUGNA = 'PUGNA',
  UNKNOWN = 'UNKNOWN',
}

export type LobbyUser = {
  lobbyId: string;
  userSteamId: string;
  role: LobbyUserRole;
  isOwner?: boolean;
};

export type LobbyFull = Lobby & {
  users: Array<LobbyUser & Record<'user', User>>;
};

export enum SocketMessages {
  DIFFICULTIES_SYNC = 'DIFFICULTIES_SYNC',

  DIFFICULTY_CHANGE = 'DIFFICULTY_CHANGE',
  DIFFICULTY_CHANGE_SUCCESS = 'DIFFICULTY_CHANGE_SUCCESS',

  DIFFICULTY_SYNC = 'DIFFICULTY_SYNC',

  LOBBY_CREATE_SYNC = 'LOBBY_CREATE_SYNC',
  LOBBY_UPDATE_SYNC = 'LOBBY_UPDATE_SYNC',
  LOBBY_DELETE_SYNC = 'LOBBY_DELETE_SYNC',

  LOBBY_CREATE_AND_JOIN = 'LOBBY_CREATE_AND_JOIN',

  LOBBY_JOIN = 'LOBBY_JOIN',
  LOBBY_JOIN_SUCCESS = 'LOBBY_JOIN_SUCCESS',

  LOBBY_START = 'LOBBY_START',
  LOBBY_START_SUCCESS = 'LOBBY_START_SUCCESS',

  LOBBY_LEAVE = 'LOBBY_LEAVE',
  LOBBY_LEAVE_SUCCESS = 'LOBBY_LEAVE_SUCCESS',

  LOBBY_PLAYER_REMOVE = 'LOBBY_PLAYER_REMOVE',

  LOBBY_ROLE_UPDATE = 'LOBBY_ROLE_UPDATE',
}

export type CreateLobbyPayload = Record<
  'lobby',
  Pick<Lobby, 'name' | 'difficultyLevel' | 'maxPlayers' | 'region'>
> &
  Pick<LobbyUser, 'role'>;
