import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindConditions, getConnection, Repository } from 'typeorm';
import { Lobby, LobbyFull, LobbyUser } from './lobby.type';
import { LobbyEntity, LobbyUserEntity } from './entities';
import { User } from '../user/user.type';
import { WsException } from '@nestjs/websockets';
import { LobbyGateway } from './lobby.gateway';

@Injectable()
export class LobbyService {
  constructor(
    @InjectRepository(LobbyEntity)
    private readonly lobbyRepository: Repository<LobbyEntity>,
    @InjectRepository(LobbyUserEntity)
    private readonly lobbyUserRepository: Repository<LobbyUserEntity>,
    @Inject(forwardRef(() => LobbyGateway))
    private readonly lobbyGateway: LobbyGateway,
  ) {}

  async deleteAllLobbies() {
    await this.lobbyRepository.delete({});
  }

  async deleteLobby(
    args: Required<Pick<Lobby, 'id'>> & Partial<Omit<Lobby, 'id'>>,
  ) {
    const lobby = await this.getLobby(args);

    await this.lobbyRepository.delete(args);

    this.lobbyGateway.onLobbyDelete(lobby);
  }

  getLobbyFull(args: Partial<Lobby>): Promise<LobbyFull> {
    return this.lobbyRepository.findOneOrFail(args, {
      relations: ['users', 'users.user'],
    });
  }

  getLobbyFullWithHidden(args: Partial<Lobby>): Promise<LobbyFull> {
    return this.lobbyRepository
      .createQueryBuilder('lobby')
      .addSelect('lobby.password')
      .leftJoinAndSelect('lobby.users', 'users')
      .leftJoinAndSelect('users.user', 'user')
      .where(args)
      .getOne();
  }

  findLobbies(args: FindConditions<Lobby>): Promise<Array<Lobby>> {
    return this.lobbyRepository.find({
      where: args,
      relations: ['users', 'users.user'],
    });
  }

  async getLobby(args: Partial<Lobby>): Promise<Lobby> {
    return this.lobbyRepository.findOneOrFail(args);
  }

  async updateLobby(
    args: Required<Pick<Lobby, 'id'>> & Partial<Omit<Lobby, 'id'>>,
  ): Promise<Lobby> {
    const lobby = await this.lobbyRepository.save(args);

    await this.lobbyGateway.onLobbyUpdate({ id: lobby.id });

    return lobby;
  }

  async updateLobbyRole(
    args: Required<Pick<LobbyUser, 'lobbyId' | 'userSteamId'>> &
      Partial<Omit<LobbyUser, 'lobbyId' | 'userSteamId'>>,
  ): Promise<LobbyUser> {
    const lobbyUser = await this.lobbyUserRepository.save(args);

    await this.lobbyGateway.onLobbyUpdate({ id: lobbyUser.lobbyId });

    return lobbyUser;
  }

  async createAndJoinLobby(args: CreateAndJoinLobbyPayload): Promise<Lobby> {
    const { lobby, user } = args;

    const queryRunner = getConnection().createQueryRunner();

    await queryRunner.startTransaction();

    const { manager } = queryRunner;

    try {
      const createdLobby = await manager.save(
        LobbyEntity,
        manager.create(LobbyEntity, lobby),
      );

      await manager.save(
        LobbyUserEntity,
        manager.create(LobbyUserEntity, {
          lobbyId: createdLobby.id,
          ...user,
          isOwner: true,
        }),
      );

      await queryRunner.commitTransaction();
      await queryRunner.release();

      await this.lobbyGateway.onLobbyCreate({ id: createdLobby.id });

      return createdLobby;
    } catch (e) {
      await queryRunner.rollbackTransaction();
      await queryRunner.release();

      throw new WsException(e);
    }
  }

  async joinLobby(
    args: Pick<LobbyUser, 'lobbyId' | 'userSteamId'>,
  ): Promise<Lobby> {
    const { lobbyId, userSteamId } = args;

    const lobby = await this.lobbyRepository.findOneOrFail({
      where: { id: lobbyId },
    });

    const { maxPlayers } = lobby;

    const currentPlayers = await this.lobbyUserRepository.count({ lobbyId });

    if (currentPlayers >= maxPlayers) {
      throw new WsException('Lobby is full');
    }

    await this.lobbyUserRepository.save(
      this.lobbyUserRepository.create({ lobbyId, userSteamId }),
    );

    this.lobbyGateway.onLobbyUpdate({ id: lobbyId });

    return lobby;
  }

  async disconnectUser(
    args: Record<'userSteamId', User['steamId']>,
  ): Promise<Lobby | void> {
    const { userSteamId } = args;

    const queryRunner = getConnection().createQueryRunner();

    await queryRunner.startTransaction();

    const { manager } = queryRunner;

    try {
      const lobbyUser = await manager.findOne(LobbyUserEntity, { userSteamId });

      if (!lobbyUser) {
        await queryRunner.commitTransaction();
        await queryRunner.release();

        return;
      }

      const { lobbyId, isOwner } = lobbyUser;

      const lobby = await manager.findOneOrFail(LobbyEntity, {
        id: lobbyId,
      });

      const { difficultyLevel: level, id } = lobby;

      const lobbyUsers = await manager.find(LobbyUserEntity, {
        where: { lobbyId },
      });

      if (lobbyUsers.length === 1) {
        await manager.delete(LobbyEntity, { id: lobbyId });

        await queryRunner.commitTransaction();
        await queryRunner.release();

        await this.lobbyGateway.onDifficultiesStatsUpdate({ level });
        this.lobbyGateway.onLobbyDelete(lobby);

        return lobby;
      }

      await manager.delete(LobbyUserEntity, { lobbyId, userSteamId });

      if (isOwner) {
        const remainingUser = await manager.findOneOrFail(LobbyUserEntity, {
          lobbyId,
        });

        manager.update(
          LobbyUserEntity,
          { lobbyId, userSteamId: remainingUser.userSteamId },
          { isOwner: true },
        );
      }

      await queryRunner.commitTransaction();
      await queryRunner.release();

      await this.lobbyGateway.onDifficultiesStatsUpdate({ level });
      this.lobbyGateway.onLobbyUpdate({ id });

      return lobby;
    } catch (e) {
      await queryRunner.rollbackTransaction();
      await queryRunner.release();

      throw new WsException(e);
    }
  }

  findOneLobbyUser(args: Partial<LobbyUser>): Promise<LobbyUser | undefined> {
    return this.lobbyUserRepository.findOne(args);
  }
}

export type CreateAndJoinLobbyPayload = Record<
  'lobby',
  Omit<Lobby, 'id' | 'createdAt' | 'isStarted' | 'password'>
> &
  Record<'user', Omit<LobbyUser, 'lobbyId' | 'isOwner'>>;
