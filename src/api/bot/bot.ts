import steam, { SteamClient, SteamUser } from 'steam';
import dota2 from 'dota2';
import {
  BehaviorSubject,
  first,
  fromEvent,
  lastValueFrom,
  Observable,
  timeout,
} from 'rxjs';
import { User } from '../user/user.type';
import Long from 'long';
import { LobbyRegion } from '../lobby/lobby.type';
import { promisify } from 'util';

export class Bot {
  private readonly steamClient: SteamClient;
  private readonly steamUser: SteamUser;
  private readonly dota2Client;

  private readonly isConnected: BehaviorSubject<boolean> = new BehaviorSubject(
    false,
  );
  private readonly isBusy: BehaviorSubject<boolean> = new BehaviorSubject(
    false,
  );

  constructor(login: string, password: string) {
    this.steamClient = new SteamClient();
    this.steamUser = new SteamUser(this.steamClient);
    this.dota2Client = new dota2.Dota2Client(this.steamClient, true);

    this.steamClient.connect();

    this.steamClient.on('connected', () => {
      this.steamUser.logOn({
        account_name: login,
        password,
      });
    });

    this.steamClient.on('logOnResponse', (logonResp) => {
      if (logonResp.eresult == steam.EResult.OK) {
        this.dota2Client.launch();

        this.dota2Client.on('ready', () => {
          this.isConnected.next(true);
        });
      }
    });

    this.steamClient.on('error', () => {
      this.isConnected.next(false);
    });
  }

  get isConnected$(): Observable<boolean> {
    return this.isConnected.asObservable();
  }

  get isBusy$(): Observable<boolean> {
    return this.isBusy.asObservable();
  }

  leaveLobby(): Promise<void> {
    return new Promise<void>((resolve) => {
      this.dota2Client.leavePracticeLobby(() => {
        resolve();
      });
    });
  }

  async createAndFillLobby(
    ownerId: User['steamId'],
    players: Array<User['steamId']> = [],
    region: LobbyRegion = LobbyRegion.STOCKHOLM,
  ): Promise<string> {
    this.isBusy.next(true);

    const password = this.makePassword(10);

    const leaveLobbyFn = promisify(this.dota2Client.leavePracticeLobby).bind(
      this.dota2Client,
    );

    try {
      const body = await promisify(this.dota2Client.createPracticeLobby).bind(
        this.dota2Client,
      )({
        custom_game_id: 2732185969,
        custom_game_mode: '2732185969',
        server_region: region,
        game_mode: 15,
        custom_map_name: 'demo',
        custom_min_players: 1,
        custom_max_players: 4,
        custom_difficulty: false,
        pass_key: password,
      });

      if (body.eresult !== 1) {
        throw new Error('Cannot create lobby');
      }

      this.dota2Client.inviteToLobby(ownerId);

      const updateResult = await lastValueFrom(
        fromEvent(this.dota2Client, 'practiceLobbyUpdate').pipe(
          timeout({ first: 60 * 1000 }),
          first(),
        ),
      );

      if (
        !updateResult['all_members'].find(({ id }) => {
          return new Long(id.low, id.high, id.unsigned).toString() === ownerId;
        })
      ) {
        throw new Error('Invite declined');
      }

      players.map((playerId) => this.dota2Client.inviteToLobby(playerId));

      await leaveLobbyFn();

      this.isBusy.next(false);

      return password;
    } catch (e) {
      await leaveLobbyFn();

      this.isBusy.next(false);

      throw e;
    }
  }

  private makePassword(length) {
    let result = '';

    const characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;

    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
  }
}
