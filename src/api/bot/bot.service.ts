import { InjectQueue } from '@nestjs/bull';
import { Injectable } from '@nestjs/common';
import { Queue } from 'bull';
import { Lobby } from '../lobby/lobby.type';
import { Jobs, QUEUE_NAME } from './constants';

@Injectable()
export class BotService {
  constructor(@InjectQueue(QUEUE_NAME) private readonly lobbyQueue: Queue) {}

  async startLobby(args: Pick<Lobby, 'id'>) {
    await this.lobbyQueue.add(Jobs.START, args);
  }

  async deleteLobby(args: Pick<Lobby, 'id'>) {
    await this.lobbyQueue.add(Jobs.DELETE, args, { delay: 5 * 60 * 1000 });
  }
}
