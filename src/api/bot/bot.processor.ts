import { Process, Processor } from '@nestjs/bull';
import { Job } from 'bull';
import { combineLatest, filter, first, lastValueFrom, map } from 'rxjs';
import { LobbyService } from '../lobby';
import { Lobby } from '../lobby/lobby.type';
import { Bot } from './bot';
import { BotService } from './bot.service';
import { Jobs, QUEUE_NAME } from './constants';

@Processor(QUEUE_NAME)
export class LobbyBotConsumer {
  private readonly bots: Array<Bot> = [];

  constructor(
    private readonly lobbyService: LobbyService,
    private readonly botService: BotService,
  ) {
    this.bots.push(new Bot('Guarding_Athena_1', `biae7x9zivrl40s2`));
  }

  async onModuleDestroy() {
    this.bots.forEach((bot) =>
      bot.isConnected$
        .pipe(
          filter((isConnected) => !!isConnected),
          first(),
        )
        .subscribe(() => {
          bot.leaveLobby();
        }),
    );
  }

  async onModuleInit() {
    this.bots.forEach((bot) =>
      bot.isConnected$
        .pipe(
          filter((isConnected) => !!isConnected),
          first(),
        )
        .subscribe(async () => {
          bot.leaveLobby();
        }),
    );
  }

  @Process(Jobs.START)
  async start(job: Job<Pick<Lobby, 'id'>>) {
    const {
      data: { id },
    } = job;

    try {
      const lobby = await this.lobbyService.getLobbyFull({
        id,
        isStarted: true,
      });

      const owner = lobby.users.find(({ isOwner }) => isOwner);

      const otherUsers = lobby.users.reduce<Array<string>>(
        (acc, { userSteamId, isOwner }) => {
          if (!isOwner) {
            acc.push(userSteamId);
          }

          return acc;
        },
        [],
      );

      if (!owner) {
        throw new Error('Lobby owner not found');
      }

      const freeBot = await lastValueFrom(
        combineLatest(
          this.bots.map((bot, index) =>
            combineLatest([bot.isConnected$, bot.isBusy$]).pipe(
              filter(([isConnected, isBusy]) => !!isConnected && !isBusy),
              map(() => this.bots[index]),
            ),
          ),
        ).pipe(
          first(),
          map((bots) => bots[0]),
        ),
      );

      const password = await freeBot.createAndFillLobby(
        owner.userSteamId,
        otherUsers,
        lobby.region,
      );

      await this.lobbyService.updateLobby({ id: lobby.id, password });

      await this.botService.deleteLobby({ id });
    } catch (e) {
      await this.lobbyService.updateLobby({ id, isStarted: false });
    }
  }

  @Process(Jobs.DELETE)
  async delete(job: Job<Pick<Lobby, 'id'>>) {
    const {
      data: { id },
    } = job;

    await this.lobbyService.deleteLobby({ id, isStarted: true });
  }
}
