import { BullModule } from '@nestjs/bull';
import { Module } from '@nestjs/common';
import { LobbyModule } from '../lobby';
import { LobbyBotConsumer } from './bot.processor';
import { BotService } from './bot.service';
import { QUEUE_NAME } from './constants';

@Module({
  imports: [
    BullModule.registerQueue({
      name: QUEUE_NAME,
    }),
    LobbyModule,
  ],
  providers: [BotService, LobbyBotConsumer],
  exports: [BotService],
})
export class BotModule {}
