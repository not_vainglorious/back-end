import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const AuthenticatedUser = createParamDecorator(
  (_: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();

    return request.user;
  },
);

export const AuthenticatedUserWs = createParamDecorator(
  (_: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToWs().getClient();

    return request.handshake.headers.user;
  },
);
