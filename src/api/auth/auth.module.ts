import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { AppConfigModule, AppConfigService } from 'src/app-config';
import { UserModule } from 'src/api/user/user.module';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtStrategy, SteamStrategy } from './strategies';
import { JwtSocketAuthGuard } from './guards/jwt-socket.guard';

@Module({
  imports: [
    PassportModule,
    AppConfigModule,
    UserModule,
    JwtModule.registerAsync({
      useExisting: AppConfigService,
      imports: [AppConfigModule],
    }),
  ],
  providers: [AuthService, SteamStrategy, JwtStrategy, JwtSocketAuthGuard],
  exports: [AuthService],
  controllers: [AuthController],
})
export class AuthModule {}
