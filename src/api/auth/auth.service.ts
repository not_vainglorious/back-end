import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { User } from 'src/api/user/user.type';
import { AppConfigService } from 'src/app-config';
import { UserService } from '../user';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly appConfigService: AppConfigService,
    private readonly userService: UserService,
  ) {}

  createToken({ steamId }: User): Promise<string> {
    return this.jwtService.signAsync({ steamId });
  }

  async verifyBearerToken({ token }: Record<'token', string>): Promise<User> {
    if (!token.startsWith('Bearer ')) {
      throw new Error('Not Bearer token');
    }

    const parsedToken = token.substring(7, token.length);

    try {
      const { steamId } = await this.jwtService.verifyAsync<
        Record<'steamId', string>
      >(parsedToken, {
        secret: this.appConfigService.jwtSecret,
      });

      return this.userService.get({ steamId });
    } catch (e) {
      throw new Error('Verify failed');
    }
  }
}
