import { ExecutionContext, Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { WsException } from '@nestjs/websockets';
import { User } from 'src/api/user/user.type';

@Injectable()
export class JwtSocketAuthGuard extends AuthGuard('jwt') {
  getRequest(context: ExecutionContext) {
    const ws = context.switchToWs().getClient();

    return {
      headers: {
        authorization: ws.handshake.headers.authorization,
      },
    };
  }

  handleRequest(err: any, user: User, info: any, context: any): any {
    if (err) {
      throw new WsException('Auth is required');
    }

    const ws = context.switchToWs().getClient();

    ws.handshake.headers.user = user;
  }
}
