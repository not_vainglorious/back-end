import {
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Res,
  UseGuards,
} from '@nestjs/common';
import { ApiTags, ApiOperation, ApiResponse } from '@nestjs/swagger';
import { Response } from 'express';
import { AppConfigService } from 'src/app-config';
import { User } from 'src/api/user/user.type';
import { AuthService } from './auth.service';
import { AuthenticatedUser } from './decorators';
import { SteamOAuthGuard } from './guards';

@Controller()
@ApiTags('Auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly appConfigService: AppConfigService,
  ) {}

  @ApiOperation({ summary: 'Steam OAuth' })
  @HttpCode(HttpStatus.FOUND)
  @Get('steam')
  @UseGuards(SteamOAuthGuard)
  steamAuth() {
    return;
  }

  @ApiOperation({ summary: 'Steam OAuth Callback' })
  @ApiResponse({ status: HttpStatus.FOUND })
  @HttpCode(HttpStatus.FOUND)
  @Get('steam/callback')
  @UseGuards(SteamOAuthGuard)
  async steamAuthCallback(
    @AuthenticatedUser() user: User,
    @Res() res: Response,
  ) {
    const token = await this.authService.createToken(user);

    res.redirect(`${this.appConfigService.websiteUrl}/callback?token=${token}`);
  }
}
