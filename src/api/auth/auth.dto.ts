import { ApiProperty } from '@nestjs/swagger';
import { UserDto } from '../user/user.dto';
import { User } from '../user/user.type';

export class SteamAuthDto
  implements Record<'token', string>, Record<'user', User>
{
  @ApiProperty()
  public readonly token: string;

  @ApiProperty({ type: UserDto })
  public readonly user: User;

  constructor(partial: Partial<SteamAuthDto>) {
    Object.assign(this, partial);
  }
}
