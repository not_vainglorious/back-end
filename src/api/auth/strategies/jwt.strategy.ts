import { Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AppConfigService } from 'src/app-config/app-config.service';
import { UserService } from 'src/api/user/user.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly appConfigService: AppConfigService,
    private readonly userService: UserService,
  ) {
    super(appConfigService.createJwtStrategyOptions());
  }

  async validate({ steamId }: Record<'steamId', string>) {
    const user = await this.userService.findOne({ steamId });

    if (!user) {
      throw new UnauthorizedException();
    }

    return user;
  }
}
