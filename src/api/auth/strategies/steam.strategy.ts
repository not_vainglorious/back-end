import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-steam';
import { Injectable } from '@nestjs/common';
import { AppConfigService } from 'src/app-config/app-config.service';
import { UserService } from 'src/api/user/user.service';

@Injectable()
export class SteamStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly appConfigService: AppConfigService,
    private readonly userService: UserService,
  ) {
    super(appConfigService.createSteamStrategyOptions());
  }

  async validate(_: string, user: SteamUser) {
    const {
      _json: {
        steamid: steamId,
        personaname: name,
        avatar: imageUrl,
        avatarmedium: imageUrlMedium,
        avatarfull: imageUrlFull,
      },
    } = user;

    const existingUser = await this.userService.findOne({ steamId });

    if (existingUser) {
      return existingUser;
    }

    const createdUser = await this.userService.create({
      steamId,
      name,
      imageUrl,
      imageUrlMedium,
      imageUrlFull,
    });

    return createdUser;
  }
}

type SteamUser = {
  provider: 'steam';
  _json: {
    steamid: string;
    communityvisibilitystate: number;
    profilestate: number;
    personaname: string;
    profileurl: string;
    avatar: string;
    avatarmedium: string;
    avatarfull: string;
    avatarhash: string;
    lastlogoff: number;
    personastate: number;
    realname: number;
    primaryclanid: string;
    timecreated: number;
    personastateflags: number;
    loccountrycode: string;
  };
  id: string;
  displayName: string;
  photos: Array<{ value: string }>;
};
