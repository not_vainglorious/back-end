import { BullModule } from '@nestjs/bull';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ApiModule } from './api';
import { AppConfigModule, AppConfigService } from './app-config';

@Module({
  imports: [
    ApiModule,
    BullModule.forRootAsync({
      imports: [AppConfigModule],
      useExisting: AppConfigService,
    }),
    TypeOrmModule.forRootAsync({
      imports: [AppConfigModule],
      useExisting: AppConfigService,
    }),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
