import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtModuleOptions } from '@nestjs/jwt';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { QueueOptions } from 'bull';
import { ExtractJwt, StrategyOptions } from 'passport-jwt';
import { Environment, EnvironmentVariables } from './env.validation';
import Redis from 'ioredis';
import { BullModuleOptions } from '@nestjs/bull';

@Injectable()
export class AppConfigService {
  constructor(private readonly configService: ConfigService) {}

  get isProduction(): boolean {
    return (
      this.configService.get(EnvironmentVariables.NODE_ENV) ===
      Environment.Production
    );
  }

  get websiteUrl(): string {
    return this.configService.get(EnvironmentVariables.WEBSITE_URL);
  }

  get jwtSecret(): string {
    return this.configService.get<string>(EnvironmentVariables.JWT_SECRET);
  }

  createTypeOrmOptions(): Promise<TypeOrmModuleOptions> | TypeOrmModuleOptions {
    return {
      type: 'postgres',
      url: this.configService.get(EnvironmentVariables.POSTGRES_URL),
      entities: ['dist/**/*.entity.js'],
      migrationsRun: this.isProduction,
    };
  }

  createSteamStrategyOptions() {
    const serverUrl = this.configService.get(EnvironmentVariables.SERVER_URL);

    return {
      returnURL: `${serverUrl}/api/v1/auth/steam/callback`,
      realm: serverUrl,
      apiKey: this.configService.get(EnvironmentVariables.STEAM_API_KEY),
    };
  }

  createJwtStrategyOptions(): Promise<StrategyOptions> | StrategyOptions {
    return {
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: this.configService.get<string>(
        EnvironmentVariables.JWT_SECRET,
      ),
    };
  }

  createJwtOptions(): Promise<JwtModuleOptions> | JwtModuleOptions {
    return {
      secret: this.configService.get<string>(EnvironmentVariables.JWT_SECRET),
      signOptions: {
        expiresIn: this.configService.get<string>(
          EnvironmentVariables.JWT_EXPIRES_IN,
        ),
      },
    };
  }

  public createSharedConfiguration(): Promise<QueueOptions> | QueueOptions {
    return {
      createClient: (): Redis.Redis =>
        new Redis(
          this.configService.get<string>(EnvironmentVariables.REDIS_URL),
          {
            maxRetriesPerRequest: null,
            enableReadyCheck: false,
          },
        ),
    };
  }

  public createBullOptions(): Promise<BullModuleOptions> | BullModuleOptions {
    return {
      name: 'lobby',
    };
  }
}
