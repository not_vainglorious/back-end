import Joi from 'joi';

export enum Environment {
  Development = 'development',
  Production = 'production',
  Test = 'test',
  Provision = 'provision',
}

export enum EnvironmentVariables {
  NODE_ENV = 'NODE_ENV',
  SERVER_URL = 'SERVER_URL',
  WEBSITE_URL = 'WEBSITE_URL',

  POSTGRES_URL = 'POSTGRES_URL',
  REDIS_URL = 'REDIS_URL',

  STEAM_API_KEY = 'STEAM_API_KEY',

  JWT_SECRET = 'JWT_SECRET',
  JWT_EXPIRES_IN = 'JWT_EXPIRES_IN',
}

export const validationSchema = Joi.object({
  [EnvironmentVariables.NODE_ENV]: Joi.string()
    .valid(...Object.values(Environment))
    .default(Environment.Production),

  [EnvironmentVariables.SERVER_URL]: Joi.string().required(),
  [EnvironmentVariables.WEBSITE_URL]: Joi.string().required(),

  [EnvironmentVariables.POSTGRES_URL]: Joi.string().required(),
  [EnvironmentVariables.REDIS_URL]: Joi.string().required(),

  [EnvironmentVariables.JWT_SECRET]: Joi.string().required(),
  [EnvironmentVariables.JWT_EXPIRES_IN]: Joi.string().required(),

  [EnvironmentVariables.STEAM_API_KEY]: Joi.string().required(),
});
